"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

require("core-js/modules/es6.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isValid = isValid;

require("core-js/modules/es6.array.index-of");

require("core-js/modules/es6.regexp.replace");

require("core-js/modules/es7.array.includes");

require("core-js/modules/es6.string.includes");

require("core-js/modules/es6.array.filter");

require("core-js/modules/es6.regexp.split");

require("core-js/modules/es6.regexp.to-string");

require("core-js/modules/es6.date.to-string");

require("core-js/modules/es6.array.map");

require("core-js/modules/es6.string.trim");

require("core-js/modules/es6.array.is-array");

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

function isValid() {
  var acronym = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var productName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  // Validate argument types
  if (typeof acronym !== 'string') {
    throw new TypeError("Expected first argument \"acronym\" to be a string, got ".concat((0, _typeof2.default)(acronym), "."));
  }

  if (!Array.isArray(productName)) {
    throw new TypeError("Expected second argument \"productName\" to be an array, got ".concat((0, _typeof2.default)(productName), "."));
  } // Normalize acronym and productName


  acronym = acronym.trim().toUpperCase();
  productName = productName.map(function (x) {
    return (typeof x === 'number' && !isNaN(x) ? x.toString() : x.trim()).toUpperCase();
  }); // Check whether the character length of the acronym 
  // exceeds the word length of the productName

  if (acronym.length < productName.length) return false;
  var parts = acronym.split('');
  var stack = []; // Check whether the character length of the acronym
  // is equal to the word length of the productName

  if (parts.length == productName.length) {
    // Validate each character of the acronym against
    // each word of the productName
    stack = parts.filter(function (char, i) {
      return productName[i].includes(char);
    });
    return parts.length == stack.length;
  } // Concatenate words of the productName array into a trimmed string


  var join = productName.join('').replace(/ /g, ''); // Stack valid characters of the acronym relative to the order
  // in which they occur within the productName string

  stack = parts.filter(function (char) {
    var i = join.indexOf(char);
    var exists = i > -1; // circumvent multiple occurrences that are indexed before the
    // current with an edge case that slices the productName string 
    // starting at the next index

    if (exists) {
      join = join.slice(i + 1, join.length);
    }

    return exists;
  }); // Concatenate stack and compare to original acronym for final validation

  return stack.join('') == acronym;
}
//# sourceMappingURL=index.js.map