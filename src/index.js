export function isValid(acronym = '', productName = []) {
  // Validate argument types
  if (typeof acronym !== 'string') {
    throw new TypeError(`Expected first argument "acronym" to be a string, got ${typeof acronym}.`);
  }
  if (!Array.isArray(productName)) {
    throw new TypeError(`Expected second argument "productName" to be an array, got ${typeof productName}.`);
  }

  // Normalize acronym and productName
  acronym = acronym.trim().toUpperCase();
  productName = productName.map(x => 
    ((typeof x === 'number' && !isNaN(x)) ? x.toString() : x.trim()).toUpperCase()
  );

  // Check whether the character length of the acronym 
  // exceeds the word length of the productName
  if (acronym.length < productName.length) return false;
  
  const parts = acronym.split('');
  let stack = [];

  // Check whether the character length of the acronym
  // is equal to the word length of the productName
  if (parts.length == productName.length) {
    // Validate each character of the acronym against
    // each word of the productName
    stack = parts.filter((char, i) => productName[i].includes(char));
    return parts.length == stack.length;
  }

  // Concatenate words of the productName array into a trimmed string
  let join = productName.join('').replace(/ /g, '');
  
  // Stack valid characters of the acronym relative to the order
  // in which they occur within the productName string
  stack = parts.filter(char => {
    const i = join.indexOf(char);
    const exists = i > -1;
    // circumvent multiple occurrences that are indexed before the
    // current with an edge case that slices the productName string 
    // starting at the next index
    if (exists) {
      join = join.slice(i + 1, join.length);
    }
    return exists;
  });
  
  // Concatenate stack and compare to original acronym for final validation
  return stack.join('') == acronym;
}