import { isValid } from '../src';

let acronyms, productName;

test('GISi Zombie Tracker 3000', () => {
  productName = [ 'GISi', 'Zombie', 'Tracker', 3000 ];
  
  acronyms = [ 'GIZTK3', 'GZT3', 'GISEE0' ];
  acronyms.forEach(acronym => {
    expect(isValid(acronym, productName)).toBeTruthy();
  });

  acronyms = [ 'GZT3k', 'GZT', 'GZTK', 'BLAH' ];
  acronyms.forEach(acronym => {
    expect(isValid(acronym, productName)).toBeFalsy();
  });
});

test('Google Search Engine', () => {
  productName = [ 'Google', 'Search', 'Engine' ];
  expect(isValid('GOOSE', productName)).toBeTruthy();
  
  acronyms = [ 'GOOSE', 'GEANIE', 'OOSN', 'LEACHE', 'OOGLEE', 'GOOSEEE', 'GGSEE', 'GGSE' ];
  acronyms.forEach(acronym => {
    expect(isValid(acronym, productName)).toBeTruthy();
  });

  acronyms = [ 'SGE', 'GEANIEOOSN', 'OGGLEE', 'GGG', 'GGSEA' ];
  acronyms.forEach(acronym => {
    expect(isValid(acronym, productName)).toBeFalsy();
  });
});