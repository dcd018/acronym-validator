# acronym-validator

An Acronym Validator written in JavaScript providing a basic utility for validating an acronym against an array of product name words

### Usage

Specify module as a dependency in `package.json`

```json
{
  "dependencies": {
    "acronym-validator": "git+https://bitbucket.org/dcd018/acronym-validator"
  }
}
```

Install dependencies

```sh
$ npm install # or yarn install
```

Import function or module as you normally would

```js
const { isValid } = require('acronym-validator');
if (isValid('GOOSE', [ 'Google', 'Search', 'Engine' ])) {
    //the acronym is valid
}
```

### Running tests

```sh
$ git clone https://bitbucket.org/dcd018/acronym-validator
$ cd acronym-validator
$ yarn jest
```